﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_02
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Are you Level 5 or Level 6? Please enter '5' or '6'");
            var level = int.Parse(Console.ReadLine());


            {
                if (level == 5)
                {
                    var stuid = new List<string> { "10003633", "" };
                    var papers = new List<string> { "", "" };

                    Console.WriteLine("You are level 5");
                    Console.WriteLine("Please enter your Student ID number");
                    stuid.Add(Console.ReadLine());

                    Console.WriteLine("please enter paper 1");
                    papers.Add(Console.ReadLine());
                    Console.WriteLine("What % did you get for this paper?");
                    int res1 = int.Parse(Console.ReadLine());

                    Console.WriteLine("Please enter paper 2");
                    papers.Add(Console.ReadLine());
                    Console.WriteLine("What % did you get for this paper?");
                    int res2 = int.Parse(Console.ReadLine());

                    Console.WriteLine("Please enter paper 3");
                    papers.Add(Console.ReadLine());
                    Console.WriteLine("What % did you get for this paper?");
                    int res3 = int.Parse(Console.ReadLine());

                    Console.WriteLine("Please enter paper 4");
                    papers.Add(Console.ReadLine());
                    Console.WriteLine("What % did you get for this paper?");
                    int res4 = int.Parse(Console.ReadLine());

                    int totalres = ((res1 + res2 + res3 + res4) / 4);
                    Console.WriteLine($"you have an overall mark of {totalres}%");


                    var overall = new List<int> {};
                    overall.Add(totalres);


                    Console.WriteLine("The overall results are:");
                    overall.ForEach(Console.WriteLine);
                    Console.WriteLine("Students that have entered their papers and results are:");
                    stuid.ForEach(Console.WriteLine);
                    
                    Console.WriteLine("The Papers entered are:");
                    papers.ForEach(Console.WriteLine);

                    marks(totalres);
                    Console.ReadKey();
                }
                else if (level == 6)
                {
                    Console.WriteLine("You are level 6");
                    var stuid6 = new List<string> { "10003633", "" };
                    var papers6 = new List<string> { "", "" };

                    Console.WriteLine("Please enter your Student ID number");
                    stuid6.Add(Console.ReadLine());

                    Console.WriteLine("please enter paper 1");
                    papers6.Add(Console.ReadLine());
                    Console.WriteLine("What % did you get for this paper?");
                    int res16 = int.Parse(Console.ReadLine());

                    Console.WriteLine("Please enter paper 2");
                    papers6.Add(Console.ReadLine());
                    Console.WriteLine("What % did you get for this paper?");
                    int res26 = int.Parse(Console.ReadLine());

                    Console.WriteLine("Please enter paper 3");
                    papers6.Add(Console.ReadLine());
                    Console.WriteLine("What % did you get for this paper?");
                    int res36 = int.Parse(Console.ReadLine());

                    int totalres6 = ((res16 + res26 + res36) / 3);
                    Console.WriteLine($"you have an overall mark of {totalres6}%");

                    var overall6 = new List<int> { };
                    overall6.Add(totalres6);
                    Console.WriteLine("The overall results are:");
                    overall6.ForEach(Console.WriteLine);
                    Console.WriteLine("Students that have entered their papers and results are:");
                    stuid6.ForEach(Console.WriteLine);
                    Console.WriteLine("The Papers entered are:");
                    papers6.ForEach(Console.WriteLine);
                    barks(totalres6);
                    Console.ReadKey();

                }
                else
                {
                    Console.WriteLine("Please enetr a valid input of either '5' or '6'");

                }
                }
            }
        


        static void marks(int totalres)
        {


            if (totalres > 90)

            {
                Console.WriteLine("You have got an 'A+' overall Congratulations!");
            }

            else if (totalres > 85)
            {
                Console.WriteLine("You have got an 'A' overall Congratulations");
            }

            else if (totalres > 80)
            {
                Console.WriteLine("You have got an 'A-' overall");
            }

            else if (totalres > 75)
            {
                Console.WriteLine("You have got a 'B+' overall");
            }

            else if (totalres > 70)
            {
                Console.WriteLine("You have got a 'B' overall");
            }

            else if (totalres > 65)
            {
                Console.WriteLine("You have got a 'B-' overall");
            }

            else if (totalres > 60)
            {
                Console.WriteLine("You have got a 'C+' overall");
            }

            else if (totalres > 55)
            {
                Console.WriteLine("You have got a 'C' overall");
            }

            else if (totalres > 50)
            {
                Console.WriteLine("You have got a 'C-'' overall");
            }

            else if (totalres > 40)
            {
                Console.WriteLine("You have got a 'D' overall");
            }

            else if (totalres > 39)
            {
                Console.WriteLine("You have got an 'E' overall");
            }
        }
       

static void barks(int totalres6)

{

    if (totalres6 > 90)

    {
        Console.WriteLine("You have got an 'A+' overall Congratulations!");
    }

    else if (totalres6 > 85)
    {
        Console.WriteLine("You have got an 'A' overall Congratulations");
    }

    else if (totalres6 > 80)
    {
        Console.WriteLine("You have got an 'A-' overall");
    }

    else if (totalres6 > 75)
    {
        Console.WriteLine("You have got a 'B+' overall");
    }

    else if (totalres6 > 70)
    {
        Console.WriteLine("You have got a 'B' overall");
    }

    else if (totalres6 > 65)
    {
        Console.WriteLine("You have got a 'B-' overall");
    }

    else if (totalres6 > 60)
    {
        Console.WriteLine("You have got a 'C+' overall");
    }

    else if (totalres6 > 55)
    {
        Console.WriteLine("You have got a 'C' overall");
    }

    else if (totalres6 > 50)
    {
        Console.WriteLine("You have got a 'C-' overall");
    }

    else if (totalres6 > 40)
    {
        Console.WriteLine("You have got a 'D' overall");
    }

    else if (totalres6 > 39)
    {
        Console.WriteLine("You have got an 'E' overall");
    }
}
}
}


